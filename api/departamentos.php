<?php
    error_reporting(E_ALL);
    define( 'acc', 1 ); 
    require_once('../Controllers/BaseController.php');
    header('Access-Control-Allow-Origin: *');

    $data = json_decode(file_get_contents('php://input'), true);

    if(isset($data)){
        $back = [ "mensaje" => "fail"];
            
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($back);
        
    }else{
    
        $accion = filter_input(INPUT_GET,"accion",FILTER_SANITIZE_STRING);

        switch ($accion) {
        
            case 'GetDepartamentos':
                $base = new BaseController();
                $departamentos = $base->getData("https://sigma-studios.s3-us-west-2.amazonaws.com/test/colombia.json");
                
                header('Access-Control-Allow-Origin: *');
                header('Content-Type: application/json; charset=utf-8');
                echo json_encode(array_keys($departamentos));
            break;

            case 'GetMunicipios':

                $departamento = filter_input(INPUT_GET,"departamento",FILTER_SANITIZE_STRING);
                $base = new BaseController();
                $data = $base->getData("https://sigma-studios.s3-us-west-2.amazonaws.com/test/colombia.json");

                //saco los municipios una vez tengo el departamento
                $municipios = ($data[$departamento]);

                header('Access-Control-Allow-Origin: *');
                header('Content-Type: application/json; charset=utf-8');
                echo json_encode($municipios);

            break;

            case 'PostContact':

                $name = filter_input(INPUT_POST,"name",FILTER_SANITIZE_STRING);
                $email = filter_input(INPUT_POST,"email",FILTER_SANITIZE_STRING);
                $state = filter_input(INPUT_POST,"state",FILTER_SANITIZE_STRING);
                $city = filter_input(INPUT_POST,"city",FILTER_SANITIZE_STRING);

                $base = new BaseController();
                $response['mensaje'] = $base->PostInsertContacto($name,$email,$state,$city);
                
                header('Access-Control-Allow-Origin: *');
                header('Content-Type: application/json; charset=utf-8');
                echo json_encode($response);
                
            break;
        }
    }

?>