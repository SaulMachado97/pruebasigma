$.ajax({
    url: "http://sigma.com/api/departamentos.php?accion=GetDepartamentos",
    responseType: 'application/json',
    success: function(data){

        console.log(data);

        $.each(data, function(index, value){
            $("#departamentos").append("<option value='"+value+"'>"+value+"</option>");
        });
    },
    error: function(){
        console.log('Algo salio mal en departamentos');
    }
});

//Cuando cambiamos el departamento se actualiza los municipios
$('#departamentos').change(function(){

    let departamento = $(this).val();

    $.ajax({
        url: "http://sigma.com/api/departamentos.php?accion=GetMunicipios&departamento="+departamento,
        responseType: 'application/json',
        success: function(data){

            console.log(data);
            $("#municipios").html('');
            $("#municipios").append("<option value='' selected disabled hidden> Seleccione un municipio</option>");

            $.each(data, function(index, value){
                $("#municipios").append("<option value='"+value+"'>"+value+"</option>");
            });
        },
        error: function(){
            console.log('Algo salio mal en municipios');
        }
    });

    console.log('El departamento es: ' + departamento);

});

//evento para enviar la informacion a base de datos
$('#formContact').submit(function(e){
    
    e.preventDefault();
    let state   = $("#departamentos").val();
    let city    = $("#municipios").val();
    let name    = $("#nombre").val();
    let email   = $("#correo").val();

    var formData = {
        'state'   : state,
        'city'    : city,
        'name'    : name,
        'email'   : email
    }

    $.ajax({
        url: "http://sigma.com/api/departamentos.php?accion=PostContact",
        type: "POST",
        dataType: 'json',
        data: formData
    })
    .done(function(data){
        console.log(data);

        alert('Registro exitoso');
        $(location).attr('href','http://sigma.com/');
        
    })
    .fail(function(xhr, textStatus, errorThrown){
        console.log(xhr);
        console.log(textStatus);
        console.log(errorThrown);

        console.log("error");
        alert("error");
    });

});