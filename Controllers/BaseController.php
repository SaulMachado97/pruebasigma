<?php

require_once("../Models/BaseModelo.php");

class  BaseController
{
    public function GetData($url){
        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return "cURL Error #:" . $err;
        } else {
            return json_decode($response, true);
        }
    }   

    public function PostInsertContacto($name, $email, $state, $city){

        $base = new BaseModelo();
        
        try{

            $today = date("Y-m-d H:i:s");
            $conect = $base->conectar();
            
            $sql = "INSERT INTO admin_sigmatest.contacts
                    (name, email, state, city, created_at, updated_at)
                    VALUES(?, ?, ?, ?, ?, ?);
            ";
            $query = $conect->prepare($sql);
    
            $query->execute(array(
                $name, $email, $state, $city, 
                $today, $today
            ));
            
            $mensaje = "Registro exitoso!";
            
        }catch(Exception $e){
            die( "Error query: " . $e->getMessage());
        }

        return $mensaje;

    }
}


