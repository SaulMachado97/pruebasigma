<!DOCTYPE html>
<html>
    
    <head>
        <title>Prueba de Desarrollo</title>
        
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="assets/css/global.css">
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/bootstrap-grid.min.css">
        <link rel="stylesheet" href="assets/css/bootstrap-reboot.min.css">
    </head>
    <body>
        
        <div class="container">
            <!--logo-->
            <div class="row">
                <img class="img-responsive center-image" src="assets/images/sigma-logo.png" alt="sigma-logo">
            </div>
            
            <div class="bloque-center">
                    <h2 class="titulos-center">Prueba de desarrollo Sigma</h2>

                    <p class="parrafos-center">
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                        Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
                        when an unknown printer took a galley of type and scrambled it to make a type specimen book. 
                        It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. 
                        It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, 
                        and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                    </p>
            </div>

            <div class="row">
                <!-- aqui esta la imagen-->
                <div class="col-sm-6 col-md-6">
                    <img id="image" class="img-fluid" src="https://sigma-studios.s3-us-west-2.amazonaws.com/test/sigma-image.png" alt="">
                </div>
    
                <div class="col-sm-6 col-md-6">
                    <!-- aqui esta el formulario-->
                    <div id="redondear" class="card efect-card">
                        <div class="card-body"> 
                            <div id="mensaje">
                                
                            </div>
                            <form id="formContact">
                                <div class="form-group">
                                  <label for="departamento">Departamento*</label>
                                    <select class="custom-select" id="departamentos" required>
                                    <option value="" selected disabled hidden>Seleccione un departamento</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                  <label for="ciudad">Ciudad*</label>
                                    <select class="custom-select" id="municipios" required>
                                    <option value='' selected disabled hidden> Seleccione un municipio</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                  <label for="nombre">Nombre*</label>
                                  <input type="text" class="form-control" id="nombre" placeholder="Saúl Machado" required>
                                </div>
                                <div class="form-group">
                                  <label for="correo">Correo*</label>
                                  <input type="email" class="form-control" id="correo" placeholder="saulmachado@mail.com" required>
                                </div>
                                <div class="center">
                                    <button id="enviar" class="btn btn-danger btn-radio btn-center">Enviar</button>
                                </div>
                            </form>
                        </div>
                      </div>
                </div>
            </div>
            

        </div>

        <!-- Javascript -->
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <!--<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>-->
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="scripts/motor.js"></script>
    </body>
</html>